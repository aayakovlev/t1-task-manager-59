package ru.t1.aayakovlev.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;

public interface Listener {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception;

}
