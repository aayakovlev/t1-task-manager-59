package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.field.DescriptionEmptyException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.NameEmptyException;
import ru.t1.aayakovlev.tm.exception.field.StatusEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.service.model.ProjectService;

import java.util.List;

@Service
public class ProjectServiceImpl extends AbstractExtendedService<Project, ProjectRepository> implements ProjectService {

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return getRepository().create(userId, name);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return getRepository().create(userId, name, description);
    }

    @NotNull
    @Override
    @Transactional
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Project resultProject = findById(userId, id);
        resultProject.setStatus(status);
        resultProject = update(resultProject);
        return resultProject;
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    @Transactional
    public Project update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

}
