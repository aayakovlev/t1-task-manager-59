package ru.t1.aayakovlev.tm.endpoint.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.DomainEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AccessDeniedException;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.service.DomainService;
import ru.t1.aayakovlev.tm.service.PropertyService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.endpoint.DomainEndpoint")
public final class DomainEndpointImpl extends AbstractEndpoint implements DomainEndpoint {

    @NotNull
    @Autowired
    private DomainService domainService;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse backupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.backupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse backupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.backupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse base64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.base64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse base64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.base64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse binaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.binaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse binarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.binarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse jsonLoadFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.jsonLoadFXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxBResponse jsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.jsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse jsonSaveFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.jsonSaveFXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxBResponse jsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.jsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse xmlLoadFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.xmlLoadFXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxBResponse xmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.xmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse xmlSaveFXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.xmlSaveFXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxBResponse xmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.xmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse yamlLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.yamlLoad();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse yamlSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        domainService.yamlSave();
        return new DataYamlSaveFasterXmlResponse();
    }

    @WebMethod
    @SneakyThrows
    public void initScheme(
            @WebParam(name = "initToken") @Nullable final String initToken
    ) throws AccessDeniedException {
        @NotNull final String token = propertyService.getDatabaseInitToken();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        domainService.initScheme();
    }

    @WebMethod
    @SneakyThrows
    public void dropScheme(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        domainService.dropScheme();
    }

}
