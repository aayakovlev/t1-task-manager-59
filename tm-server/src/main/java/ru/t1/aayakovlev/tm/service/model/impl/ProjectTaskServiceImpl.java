package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.service.model.ProjectTaskService;

import java.util.List;

@Service
public class ProjectTaskServiceImpl implements ProjectTaskService {

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @Nullable final Project project = getProjectRepository().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        resultTask = getTaskRepository().findById(userId, taskId);
        if (resultTask == null) throw new TaskNotFoundException();
        resultTask.setProject(project);
        getTaskRepository().update(userId, resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @Nullable final Project project = getProjectRepository().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        resultTask = getTaskRepository().findById(userId, taskId);
        if (resultTask == null) throw new TaskNotFoundException();
        resultTask.setProject(null);
        getTaskRepository().update(userId, resultTask);
        return resultTask;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @Nullable Project project = getProjectRepository().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        getTaskRepository().removeAllByProjectId(userId, projectId);
        getProjectRepository().removeById(userId, projectId);
    }

    @Override
    @Transactional
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final List<Project> projects = getProjectRepository().findAll(userId);
        for (@NotNull final Project project : projects) {
            getTaskRepository().removeAllByProjectId(userId, project.getId());
        }
        getProjectRepository().clear(userId);
    }

}
