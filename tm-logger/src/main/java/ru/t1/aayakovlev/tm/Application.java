package ru.t1.aayakovlev.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.aayakovlev.tm.component.LoggerBootstrap;
import ru.t1.aayakovlev.tm.config.LoggerConfig;

public final class Application {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfig.class);
        @NotNull final LoggerBootstrap bootstrap = context.getBean(LoggerBootstrap.class);
        bootstrap.init();
    }

}
