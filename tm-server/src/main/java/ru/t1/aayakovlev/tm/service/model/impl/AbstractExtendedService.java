package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.model.ExtendedRepository;
import ru.t1.aayakovlev.tm.service.model.ExtendedService;

import java.util.*;

@Service
public abstract class AbstractExtendedService<E extends AbstractUserOwnedModel, R extends ExtendedRepository<E>>
        extends AbstractBaseService<E, R> implements ExtendedService<E> {

    @Nullable
    protected abstract ExtendedRepository<E> getRepository();

    @NotNull
    @Override
    @Transactional
    public E save(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        return getRepository().save(userId, model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> add(@Nullable final String userId, @Nullable final Collection<E> models)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        for (@NotNull final E entity : models) {
            @NotNull final E resultEntity = getRepository().save(userId, entity);
            resultEntities.add(resultEntity);
        }
        return resultEntities;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        getRepository().clear(userId);
    }

    @Override
    public int count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().count(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existById(userId, id);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (comparator == null) return findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable E resultEntity;
        resultEntity = getRepository().findById(userId, id);
        if (resultEntity == null) throw new EntityNotFoundException();
        return resultEntity;
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        removeById(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new EntityNotFoundException();
        getRepository().removeById(userId, id);

    }

    @NotNull
    @Override
    @Transactional
    public E update(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new EntityNotFoundException();
        return update(model);
    }

}
