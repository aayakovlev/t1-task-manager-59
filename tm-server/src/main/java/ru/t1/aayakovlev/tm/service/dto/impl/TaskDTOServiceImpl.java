package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;

import java.util.List;

@Service
public class TaskDTOServiceImpl extends AbstractExtendedDTOService<TaskDTO, TaskDTORepository>
        implements TaskDTOService {

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return getRepository().create(userId, name);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return getRepository().create(userId, name, description);

    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable TaskDTO resultTask = findById(userId, id);
        resultTask.setStatus(status);
        resultTask = update(resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable TaskDTO model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

}
