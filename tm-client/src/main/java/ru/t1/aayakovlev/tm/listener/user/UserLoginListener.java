package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserLoginRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Login user session.";

    @NotNull
    public static final String NAME = "login";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER LOGIN]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();
        System.out.print("Enter password: ");
        @NotNull final String password = nextLine();

        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);

        @Nullable String token = authEndpoint.login(request).getToken();
        setToken(token);
        System.out.println(token);
    }

}
