package ru.t1.aayakovlev.tm.repository.model.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.model.BaseRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractBaseRepository<E extends AbstractModel> implements BaseRepository<E> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<E> getClazz();

    @NotNull
    protected abstract String getSortColumnName(@NotNull final Comparator comparator);

    @Override
    public E save(@NotNull E entity) {
        entityManager.persist(entity);
        entityManager.flush();
        return entity;
    }

    @Override
    public void clear() {
        @NotNull final String query = "delete from " + getClazz().getSimpleName();
        entityManager.createQuery(query).executeUpdate();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findById(id) != null;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final String query = "from " + getClazz().getSimpleName();
        return entityManager.createQuery(query, getClazz())
                .getResultList();

    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " " +
                "order by " + getSortColumnName(comparator);
        return entityManager.createQuery(query, getClazz())
                .getResultList();
    }

    @Override
    @Nullable
    public E findById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Override
    public int count() {
        @NotNull final String query = "select count(*) from " + getClazz().getSimpleName();
        return entityManager.createQuery(query, Long.class)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.id = :id";
        entityManager.createQuery(query)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public E update(@NotNull final E entity) {
        entityManager.merge(entity);
        entityManager.flush();
        return entity;
    }

}
