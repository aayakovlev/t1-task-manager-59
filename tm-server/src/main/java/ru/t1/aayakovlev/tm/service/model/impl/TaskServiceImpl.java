package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.service.model.TaskService;

import java.util.List;

@Service
public class TaskServiceImpl extends AbstractExtendedService<Task, TaskRepository>
        implements TaskService {

    @Getter
    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return getRepository().create(userId, name);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return getRepository().create(userId, name, description);

    }

    @NotNull
    @Override
    @Transactional
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Task resultTask = findById(userId, id);
        resultTask.setStatus(status);
        resultTask = update(resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public Task update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Task model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

}
